package-update-indicator (7-1ubports1) focal; urgency=medium

  * UBPorts packaging

 -- Guido Berhoerster <guido+ubports@berhoerster.name>  Tue, 05 Oct 2021 16:07:15 +0200

package-update-indicator (7-1) unstable; urgency=medium

  [ Matthias Klumpp ]
  * Update gbp.conf

  [ Unit 193 ]
  * New upstream version 7.
  * d/control: Add myself as an uploader.
  * d/watch: Bump to version 4.
  * d/s/local-options: Drop, these are default now.
  * Update Standards-Version to 4.5.1.

 -- Unit 193 <unit193@debian.org>  Mon, 21 Dec 2020 20:16:18 -0500

package-update-indicator (5-2) unstable; urgency=medium

  * Team upload.
  [ Matthias Klumpp ]
  * Drop pk-update-icon transitional package

  [ Unit 193 ]
  * d/control: libappindicator3-dev → libayatana-appindicator3-dev.
    (Closes: #956777)

 -- Laurent Bigonville <bigon@debian.org>  Thu, 30 Jul 2020 10:57:33 +0200

package-update-indicator (5-1) unstable; urgency=medium

  * New upstream version: 5
  * Run make explicitly to account for variables in hand-made
    Makefile, pass prefix (Closes: #946634)
  * Bump standards version: No changes needed
  * Mark rules as not requiring root

 -- Matthias Klumpp <mak@debian.org>  Thu, 20 Feb 2020 22:34:28 +0100

package-update-indicator (4-2) unstable; urgency=medium

  * Set GPK Update Viewer as default updater command

 -- Matthias Klumpp <mak@debian.org>  Tue, 30 Jul 2019 00:44:06 +0200

package-update-indicator (4-1) unstable; urgency=medium

  * New upstream version: 4
  * Bump dh compat level to 12
  * Update standards version: No changes needed
  * Update d/watch file

 -- Matthias Klumpp <mak@debian.org>  Tue, 30 Jul 2019 00:08:06 +0200

package-update-indicator (2.0-1) unstable; urgency=medium

  * Initial release, replacing pk-update-icon (Closes: #906839)
  * Split out apt-config-auto-update to its own source package,
    for the people who still need it.

 -- Matthias Klumpp <mak@debian.org>  Sun, 16 Dec 2018 16:36:44 +0100

pk-update-icon (2.0.0-2) unstable; urgency=medium

  * Update debian/watch file
  * Bump compat-level and standards-version (no changes needed)
  * Use secure Vcs-* URLs
  * Depend on gnome-packagekit (instead of gnome-packagekit-session)

 -- Matthias Klumpp <mak@debian.org>  Sun, 22 Jan 2017 20:11:45 +0100

pk-update-icon (2.0.0-1) unstable; urgency=medium

  * New upstream release: 2.0.0

 -- Matthias Klumpp <mak@debian.org>  Wed, 12 Aug 2015 14:36:26 +0200

pk-update-icon (1.0.0-3) unstable; urgency=medium

  * Don't use deprecated Apt options (Closes: #778869)
  * Build-depend on docbook-xsl-ns instead of docbook-xsl
    - Closes: #779257
  * a-c-auto-update: Break/Replace update-notifier-common
    - Closes: #779661

 -- Matthias Klumpp <mak@debian.org>  Sat, 25 Apr 2015 15:08:06 +0200

pk-update-icon (1.0.0-2) unstable; urgency=medium

  * Add config snippets for Apt to enable auto-cache-updates

 -- Matthias Klumpp <mak@debian.org>  Mon, 24 Nov 2014 22:28:34 +0100

pk-update-icon (1.0.0-1) unstable; urgency=medium

  * Initial release (Closes: #768471, #710355)

 -- Matthias Klumpp <mak@debian.org>  Fri, 07 Nov 2014 16:08:18 +0100
